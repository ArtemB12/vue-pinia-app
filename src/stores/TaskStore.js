import { defineStore } from "pinia";

export const useTaskStore = defineStore('taskStore', {
    state: () => ({
        tasks: [],
        isLoading: false
    }),
    getters: {
        favs() {
            return this.tasks.filter(t => t.isFave)
        },
        favsCount() {
            return this.tasks.reduce((previous, current) => {
                return current.isFave ? previous + 1 : previous
            }, 0)
        },
        totalCount: (state) => {
            return state.tasks.length
        }
    },
    actions: {
        async getTasks() {
            this.isLoading = true
            const response = await fetch('http://localhost:3000/tasks')
            const data = await response.json()

            this.tasks = data
            this.isLoading = false
        },
        async addTask(task) {
            this.tasks.push(task)

            const response = await fetch('http://localhost:3000/tasks', {
                method: 'POST',
                body: JSON.stringify(task),
                headers: {'Content-Type': 'application/json'}
            })

            if (response.error) {
                console.log(response.error)
            }
        },
        async deleteTask(id) {
            this.tasks = this.tasks.filter(t => {
                return t.id !== id
            })

            const response = await fetch('http://localhost:3000/tasks/' + id, {
                method: 'DELETE',
            })

            if (response.error) {
                console.log(response.error)
            }
        },
        async toggleFav(id) {
            const task = this.tasks.find(t => t.id === id)
            task.isFave = !task.isFave

            const response = await fetch('http://localhost:3000/tasks/' + id, {
                method: 'PATCH',
                body: JSON.stringify({isFave: task.isFave}),
                headers: {'Content-Type': 'application/json'}
            })

            if (response.error) {
                console.log(response.error)
            }
        }
    }
})