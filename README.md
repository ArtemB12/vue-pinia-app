# pinia-app

Basic ToDo app with a Pinia store

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### JSON Live

Check if you have json-server to broadcast db.json

```sh
npm install json-server + json-server -w ./data/db.json 

```